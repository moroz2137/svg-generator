# SVGGenerator

This project is a proof of concept of SVG generation using EEX.

## Installation

This application has no dependencies.

## Usage

```elixir
number_of_steps = 250000
svg = SVGGenerator.GroupSteps.sample(number_of_steps)
Path.expand("../svg-demo.svg")
|> File.write(svg)
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/svg_generator](https://hexdocs.pm/svg_generator).

