defmodule SVGGenerator do
  @moduledoc """
  Documentation for SVGGenerator.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SVGGenerator.hello()
      :world

  """
  def hello do
    :world
  end
end
