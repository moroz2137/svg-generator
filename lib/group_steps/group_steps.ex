defmodule SVGGenerator.GroupSteps do
  require EEx

  template_file = File.cwd!() |> Path.join("lib/templates/mission-markers.svg.eex")
  @external_resource template_file
  EEx.function_from_file(:def, :sample, template_file, [:steps])

  def marker_class(steps, threshold)
      when is_integer(steps) and is_integer(threshold) and steps >= threshold do
    "marker is-active"
  end

  def marker_class(_, _), do: "marker"
end
